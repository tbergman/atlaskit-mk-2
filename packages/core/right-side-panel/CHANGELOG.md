# @atlaskit/right-side-panel

## 0.2.0

### Minor Changes

- [minor][617591dd61](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/617591dd61):

  Added props onOpenAnimationFinished and onCloseAnimationFinished to execute function once the open/close animation finished

## 0.1.8

### Patch Changes

- [patch][097b696613](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/097b696613):

  Components now depend on TS 3.6 internally, in order to fix an issue with TS resolving non-relative imports as relative imports

## 0.1.7

### Patch Changes

- [patch][0d7d459f1a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0d7d459f1a):

  Fixes type errors which were incompatible with TS 3.6

## 0.1.6

### Patch Changes

- [patch][ecca4d1dbb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ecca4d1dbb):

  Upgraded Typescript to 3.3.x

## 0.1.5

### Patch Changes

- [patch][de35ce8c67](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/de35ce8c67):

  Updates component maintainers

## 0.1.4

### Patch Changes

- [patch][f34776be97](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f34776be97):

  Type definition files are now referenced in package.json

## 0.1.3

### Patch Changes

- [patch][29a1f158c1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/29a1f158c1):

  Use default react import in typescript files.

## 0.1.2

### Patch Changes

- [patch][06940860ea](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/06940860ea):

  Added unit test

## 0.1.1

### Patch Changes

- [patch][a19a3616a6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a19a3616a6):

  Update examples
