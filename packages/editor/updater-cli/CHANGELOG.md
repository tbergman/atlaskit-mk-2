# @atlaskit/updater-cli

## 2.0.5

### Patch Changes

- [patch][f5a3c7e7b9](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f5a3c7e7b9):

  CLI's should return a non-zero return code when a error was thrown

## 2.0.4

### Patch Changes

- [patch][097b696613](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/097b696613):

  Components now depend on TS 3.6 internally, in order to fix an issue with TS resolving non-relative imports as relative imports

## 2.0.3

- [patch][0a4ccaafae](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0a4ccaafae):

  - Bump tslib

- [patch][0ac39bd2dd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0ac39bd2dd):

  - Bump tslib to 1.9

## 2.0.2

- [patch][d13fad66df](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d13fad66df):

  - Enable esModuleInterop for typescript, this allows correct use of default exports

## 2.0.1

- [patch][25544ecbc8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/25544ecbc8):

  - Ignore tsconfig from being published

## 2.0.0

- [major][9d5cc39394](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9d5cc39394):

  - Dropped ES5 distributables from the typescript packages

## 1.0.1

- [patch][dd30b8f831](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/dd30b8f831):

  - Add missing dependencies

## 1.0.0

- [major][bc47f570b2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bc47f570b2):

  - MVP of editor updater cli
