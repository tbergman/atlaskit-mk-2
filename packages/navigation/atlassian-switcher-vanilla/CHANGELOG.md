# @atlaskit/atlassian-switcher-vanilla

## 1.0.0

### Major Changes

- [major][fa67fac4c0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/fa67fac4c0):

  Improving docs

## 0.0.1

### Patch Changes

- [patch][a9bf2f8d31](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a9bf2f8d31):

  Adding vanilla wrapper for Atlassian switcher
