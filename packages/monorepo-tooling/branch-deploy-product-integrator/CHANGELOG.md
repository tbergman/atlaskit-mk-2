# @atlaskit/branch-deploy-product-integrator

## 1.0.2

### Patch Changes

- [patch][f5a3c7e7b9](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f5a3c7e7b9):

  CLI's should return a non-zero return code when a error was thrown

## 1.0.1

### Patch Changes

- [patch][a955d95ac4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a955d95ac4):

  Added option to run yarn dedupe at the end

## 1.0.0

### Major Changes

- [major][348b1058fd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/348b1058fd):

  First version of branch deploy integrator cli
