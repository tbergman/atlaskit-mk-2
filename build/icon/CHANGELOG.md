# @atlaskit/icon-build-process

## 0.1.1

### Patch Changes

- [patch][097b696613](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/097b696613):

  Components now depend on TS 3.6 internally, in order to fix an issue with TS resolving non-relative imports as relative imports

## 0.1.0

- [minor][b29bec1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b29bec1):

  - Change the format for the docs file to be focused on metadata only

## 0.0.1

- [patch] Update to use babel-7 for build processes [e7bb74d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7bb74d)
